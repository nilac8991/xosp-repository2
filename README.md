#######################''XOSP Is A Project licensed under Apache License''#####################################

**XDA Links**

- Samsung Galaxy S+ (ariesve) http://forum.xda-developers.com/galaxy-s-i9000/i9001-development/rom-s-reborn-beta-1-t3032125
- Samsung Galaxy W (ancora)  http://forum.xda-developers.com/galaxy-w/development/rom-s-reborn-beta-1-t3033075


**Development status for ariesve_cm12_sources branch**

- Nothing for now


**Development status for rom_custom_sources branch**

- Change the theme icon with the one provided by facco (This for later)
- Ask also to create a new bootanimation (More material style)
- Implement new header in settings for the Reborn preferences

**Development status for XPERIA_APPS branch**

- Nothing for now

**Development status for Settings.apk branch**

- Add translations for the modified strings and values (This is for later)